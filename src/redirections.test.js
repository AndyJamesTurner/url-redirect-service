const Redirections = require('./redirections');

test('jifflr on iso', () => {
    
    const app = 'jifflr'
    const platform = 'ios'

    const redirectionUrl = Redirections.getRedirectUrl(app, platform)

    expect(redirectionUrl).toBe('https://apps.apple.com/gb/app/jifflr/id1434427409')
});