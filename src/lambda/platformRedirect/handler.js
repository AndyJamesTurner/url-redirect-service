const RedirectionsTable = require('../../dynamoDb/tables/redirections')
const Redirections = require('../../redirections')
const UserAgent = require('../../userAgentDetails')

module.exports.redirect = (event, context, callback) => {
  
  // used for selecting the specific app store page to redirect to 
  const platform = getPlatform(event)
  const appName = getAppName(event)
  
  // log the redirection to the redirections table
  RedirectionsTable.incrementRedirectionCount(appName, platform)
  .catch(err => {
    console.error("failed to log entry in redirections table")
    console.error(err)
  });

  // get the redirection url for the given app and platform
  const redirectionUrl = Redirections.getRedirectUrl(appName, platform)

  // redirection response
  const response = {
    statusCode: 301,
    headers: {
      Location: redirectionUrl,
    }
  };

  return callback(null, response);
};


// gets the specified app name from either the query 
// string or path parameter
const getAppName = (event) => {

  // check if app name is provided in queryStringParameters 
  if(event.queryStringParameters && event.queryStringParameters.app) {
    return event.queryStringParameters.app
  }

  // check if app name is provided in pathParameters
  else if(event.pathParameters && event.pathParameters.app) {
    return event.pathParameters.app
  }

  // error, app name not provided
  else {
    throw 'app name not specified in either queryStringParameters or pathParameters'
  }
}

// gets the platform the user is accessing the app from
const getPlatform = (event) => {
  
  // get the user agent details from the event headers
  const headers = event.headers
  const userAgentString = headers ? headers["User-Agent"] : ""
  
  const userAgentDetails = UserAgent.getUserAgentDetails(userAgentString)
  const platform = userAgentDetails.platform

  return platform
}
