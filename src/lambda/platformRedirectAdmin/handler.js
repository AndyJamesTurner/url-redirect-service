const RedirectionsTable = require('../../dynamoDb/tables/redirections')

module.exports.index = (event, context, callback) => {
  
  // get all of the entries in the requests document
  RedirectionsTable.getAll()
  .then(entries => {

    // return the request formatted in a html table
    const response = {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/html',
      },
      body: createHtmlTableOfRedirectionCounts(entries)
    }
  
    return callback(null, response)
  })
  .catch(error => {

    console.error(error)

    // return an error page
    const response = {
      statusCode: 200,
      body: 'Error retrieving entries...'
    }
  
    return callback(null, response)
  })
}

// TODO: this is stub code to demonstrate accessing the stored
// redirection counts. Given more time this could be built
// as a static site and deployed to S3. The 'entries' would
// then be retried using a request to a serverless call.
const createHtmlTableOfRedirectionCounts = (entries) => {

  let table = ''
  table += '<html>\n'

  table += '<head>\n'
  table += '<style>\n'
  table += 'table, th, td {\n'
  table += 'border: 1px solid black;\n'
  table += '}\n'
  table += '</style>\n'
  table += '</head>\n'

  table += '<body>\n'

  table += '<table style="width:100%">\n'
  table += '<tr>\n'
  table += '<th>AppName</th>\n'
  table += '<th>Platform</th>\n'
  table += '<th>RedirectionCount</th>\n'
  table += '</tr>\n'

  entries.forEach(entry => {

    table += '<tr>\n'

    table += `<td>${entry.AppName}</td>\n`
    table += `<td>${entry.Platform}</td>\n`
    table += `<td>${entry.RedirectionCount}</td>\n`
  
    table += '</tr>\n'
  })

  table += '</table>'
  table += '</body>\n'
  table += '</html>'

  return table
}
