// returns an URL to the appropriate app store or
// website for the given the application and platform.
module.exports.getRedirectUrl = (appName, platform) => {

    const appEntry = redirections[appName]

    if(appEntry) {

        // use the specified platform if an entry is present,
        // otherwise fall back to 'other'
        const platformRedirection = appEntry[platform] || appEntry['other']

        if(platformRedirection) {
            return platformRedirection
        }
        else {            
            throw `No 'other' entry is present in the redirections dictionary for '${appName}'`
        }
    }
    else {        
        throw `No entry is present in the redirections dictionary for '${appName}'`
    }
}

// This dictionary can be extended to include
// additional apps and platforms
// Note: this dictionary could be moved to dynamoDb to allow 
// extension / changes without redeploying
// Note: all app entries must provide a 'other' platform entry 
// which is used as a fallback for unknown platforms
const redirections = {
    jifflr: {
        ios: 'https://apps.apple.com/gb/app/jifflr/id1434427409',
        android: 'https://play.google.com/store/apps/details?id=uk.jifflr.app&hl=en_GB',
        other: 'https://jifflr.com/'
    }
}
