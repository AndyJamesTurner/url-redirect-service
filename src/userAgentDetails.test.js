const UserAgent = require('./userAgentDetails');

test('Apple iPhone 8 user agent string', () => {
    
    // https://deviceatlas.com/blog/list-of-user-agent-strings
    const userAgentString = 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B350'

    const userAgentDetails = UserAgent.getUserAgentDetails(userAgentString)
    const platform = userAgentDetails.platform

    expect(platform).toBe('ios')
});

test('samsung gallery S9 user agent string', () => {
    
    // https://deviceatlas.com/blog/list-of-user-agent-strings
    const userAgentString = 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G960F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.84 Mobile Safari/537.36'

    const userAgentDetails = UserAgent.getUserAgentDetails(userAgentString)
    const platform = userAgentDetails.platform

    expect(platform).toBe('android')
});

test('Windows 10 using Edge browser user agent string', () => {
    
    // https://deviceatlas.com/blog/list-of-user-agent-strings
    const userAgentString = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246'

    const userAgentDetails = UserAgent.getUserAgentDetails(userAgentString)
    const platform = userAgentDetails.platform

    expect(platform).toBe('other')
});