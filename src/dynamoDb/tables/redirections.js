const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB();
const documentClient = new AWS.DynamoDB.DocumentClient()

// increment the RedirectionCount for the given app and platform 
module.exports.incrementRedirectionCount = (appName, platform) => {

  return new Promise((resolve, reject) => {

    getRedirectionCount(appName, platform)
      .then(redirectionCount => {

        const key = createKey(appName, platform)

        const params = {
          TableName: process.env.REDIRECTIONS_TABLE_NAME,
          Key: {
            "Key": key
          },
          UpdateExpression: "set RedirectionCount = :count",
          ExpressionAttributeValues: {
            ":count": redirectionCount + 1,
          },
        }

        documentClient.update(params, (error, data) => {

          if (error) {
            reject(error)
          }
          else {
            resolve(data)
          }
        })
      })
  })
}

// returns the redirection counts for all of the 
// app-platform pairs
module.exports.getAll = () => {

  const params = {
    TableName: process.env.REDIRECTIONS_TABLE_NAME,
    ProjectionExpression: "AppName, Platform, RedirectionCount",
  }

  return new Promise((resolve, reject) => {

    documentClient.scan(params, (error, data) => {

      if (error) {
        reject(error)
      }
      else {
        resolve(data.Items)
      }
    })
  })
}


// create the key used by the redirections entries
const createKey = (appName, platform) => {
  return `${appName}-${platform}`
}

// creates a new entry for a given app, platform pair and 
// initializes the RedirectionCount to zero
const initializeItem = (appName, platform) => {

  const key = createKey(appName, platform)

  const params = {
    TableName: process.env.REDIRECTIONS_TABLE_NAME,
    Item: {
      'AppName': { S: appName },
      'Platform': { S: platform },
      'RedirectionCount': { N: '0' },
      'Key': { S: key }
    }
  }

  return new Promise((resolve, reject) => {

    dynamoDb.putItem(params, (error, data) => {

      if (error) {
        reject(error)
      }
      else {
        resolve(key)
      }
    })
  })
}

// get the number of times a redirection link has been
// generated for a given app, platform pair
const getRedirectionCount = (appName, platform) => {

  const key = createKey(appName, platform)

  const params = {
    TableName: process.env.REDIRECTIONS_TABLE_NAME,
    Key: {
      "Key": {
        S: key
      },
    },
  };

  return new Promise((resolve, reject) => {

    dynamoDb.getItem(params, (error, data) => {

      if (error) {
        reject(error)
      }

      // there is already an entry stored for this app, platform pair
      if (data.Item) {
        resolve(parseInt(data.Item.RedirectionCount.N))
      }
      // no current entry for this app, platform pair
      else {

        // create a new entry
        initializeItem(appName, platform)
          .then((key) => {
            resolve(0)
          })
      }
    })
  })
}
