// TODO: explore if there are external libraries which can
//  be used to extract user data from the user Agent String

// returns an object of details extracted from the given 
// userAgentString. Crudely only provides the user's platform
// but can be extend to included more details i.e. OS.
module.exports.getUserAgentDetails = (userAgentString) => {

    return {
        platform: getPlatform(userAgentString)
    }
}

const getPlatform = (userAgentString) => {

    // this can be extended to support other platforms
    if(isIOS(userAgentString)) {
        return "ios"
    }
    else if(isAndroid(userAgentString)) {
        return "android"
    }
    else {
        return "other"
    }
}

// https://stackoverflow.com/questions/9038625/detect-if-device-is-ios
const isIOS = (userAgent) => /iPad|iPhone|iPod/.test(userAgent)
const isAndroid = (userAgent) => /Android/.test(userAgent)
