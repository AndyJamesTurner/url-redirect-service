Serverless application for redirecting app URLs to their corresponding platform market place URL.

# Services

Once deployed to production two services are provided.

## Redirection

Redirection to a specific platform market place based on the users current platform and specified application.

Example: ```api-gateway-url/prod/{appName}```

```{appName}``` - name of the app to be redirected for. 

Currency supported platforms: 'ios', 'android' and 'other'.

## Administration

View the number of redirections which have been used for each app and user platform pair.

Example: ```api-gateway-url/prod/admin```

# Adding new Apps and Platforms

To add a new app to the redirection service add new entries to the ```redirections``` dictionary in ```/src/redirections.js```. 

Always ensure an 'other' entry is provided as this is used as the default in cases where specific platforms are not available. 

# Architecture 

The Serverless framework is used to provision lambda functions which are exposed using API Gateway. DynamoDb is used for storing the number of redirections which have been made for each app, platform pair.


# Deploying

The services are automatically deployed to a staging environment whenever commits are made against the dev branch (provided all tests first pass). Similarly, the services are deployed to a production environment whenever commits are made against the master branch.

The AWS 'Serverless' IAM User is used for authentication and permissions when automatically deploying.

## Locally

To locally trigger deploys to the staging or production environment first set the serverless framework credentials: https://www.serverless.com/framework/docs/getting-started/.

Then:

Deploy staging: ```serverless deploy --stage dev```

Deploy production: ```serverless deploy --stage prod```

# Testing

## Javascript unit tests

Javascript unit tests can be ran by:

```
npm install
npm run test
```
  
This will provide high level test statistics in the terminal. Detailed test coverage can be access in the generated report ```coverage/lcov-report/index.html```

## Deployed lambda functions

Deployed lambda functions can be tested locally using the serverless framework: https://www.serverless.com/framework/docs/getting-started/

### Redirection Test

```serverless invoke -f platform-redirect -l --data '{ "queryStringParameters": {"app":"jifflr"}}'```

### Admin Page Test

```serverless invoke -f platform-redirect-admin -l```
